let myStorage = window.localStorage;

let app = new Vue({
    el: "#vueApp",
    data: {
        title: 'Vue To Do App',
        lists: [
            { id: 1, item: 'Get started!', isCompleted: false },
        ],
        newItem: "",
    },
    methods: {
        addItem: function () {
            let id = this.lists.length + 1
            if (this.newItem) {
                const newListItem = {id:id, item:this.newItem, isCompleted: false}
                this.lists.push(newListItem)
                // clear text input
                this.newItem = ""
            }
        },
        removeItem: function (itemId) {
            // return new array with clicked item from lists array removed
            this.lists = this.lists.filter(item => item.id !== itemId)
        },
        toggleComplete: function (itemId) {
            // change the object to change the property of lists so that the watch sees it. 
            // properties are not reactice. Below works for arrays
            // Vue.set(vm.items, indexOfItem, newValue)
            for (let i = 0; i < this.lists.length; i++) {
                const listItem = this.lists[i];
                if (listItem.id == itemId) {
                    listItem.isCompleted = !listItem.isCompleted
                    Vue.set(this.lists, i, listItem)
                }
            }
        },
    },
    mounted() {
        // load json
        let storedList = JSON.parse(myStorage.getItem('lists'))
        if (storedList) {
            this.lists = storedList
        }
    },
    watch: {
        // vue js watchers for when removing and adding items
        lists: function () {
            myStorage.clear()
            myStorage.setItem('lists', JSON.stringify(this.lists))
        }
    }
});
